﻿using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcServer.Services
{
    public class Client : Customer.CustomerBase
    {
        private readonly ILogger<Client> logger;

        public Client(ILogger<Client> logger)
        {
            this.logger = logger;
        }     
        
        public override Task<ClientModel> GetCustomerInfo(CustomerLookupModel request, ServerCallContext context)
        {
            ClientModel output = new ClientModel();

            if(request.UserId == 1)
            {
                output.FirstName = "Lorena";
                output.LastName = "Anghel";
            }
            else if (request.UserId == 2)
            {
                output.FirstName = "Ioana";
                output.LastName = "Bedreag";
            }
            else
            {
                output.FirstName = "Burtea";
                output.LastName = "Stefania";
            }

            return Task.FromResult(output);
        }

        public override async Task GetNewCustomers(
            NewCustomerRequest request,
            IServerStreamWriter<ClientModel> responseStream, 
            ServerCallContext context)
        {
            List<ClientModel> customers = new List<ClientModel>
           {
               new ClientModel
               {
                   FirstName = "Alin",
                   LastName = "Avram",
                   EmailAddress = "alin@iamalin.com",
                   Age = 25,
                   IsAlive = true
               },
               new ClientModel
               {
                   FirstName = "Lorena",
                   LastName = "Anghel",
                   EmailAddress = "lorena@yahoo.com",
                   Age = 20,
                   IsAlive = true
               }
           };

            foreach(var cust in customers)
            {
                await responseStream.WriteAsync(cust);
            }
        }
    }
}
